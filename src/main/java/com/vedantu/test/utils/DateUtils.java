package com.vedantu.test.utils;


import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class DateUtils {

    static DateTimeFormatter dateTimeFormatter =DateTimeFormatter.ofPattern("yyyy.MM.dd.HH.mm.ss");
    static  DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy.mm.dd");

    public static String getFormattedCurrentDateTime(String format) {
        if(format==null) {
            return dateTimeFormatter.format(LocalDateTime.now());
        }

        else {
            DateTimeFormatter formatter= DateTimeFormatter.ofPattern(format);
            return formatter.format(LocalDateTime.now());
        }
    }

    public static String getFormattedCurrentDate(String format) {
        if(format==null) {
            return dateFormatter.format(LocalDate.now());
        }

        else {
            DateTimeFormatter formatter= DateTimeFormatter.ofPattern(format);
            return formatter.format(LocalDate.now());
        }
    }


    public static String getFormattedDate(String format, LocalDate localDate) {
        if(format==null) {
            return dateFormatter.format(localDate);
        }

        else {
            DateTimeFormatter formatter= DateTimeFormatter.ofPattern(format);
            return formatter.format(localDate);
        }
    }

    public static String getFormattedDate(String format, LocalDateTime localDateTime) {
        if(format==null) {
            return dateTimeFormatter.format(localDateTime);
        }

        else {
            DateTimeFormatter formatter= DateTimeFormatter.ofPattern(format);
            return formatter.format(localDateTime);
        }
    }


}
