package com.vedantu.test.Controller;

import com.vedantu.test.entity.OrderDetails;
import com.vedantu.test.exception.NoInventoryException;
import com.vedantu.test.exception.OrderCreationException;
import com.vedantu.test.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpServerErrorException;

@Component
@RestController
@RequestMapping(value = "/order")
public class OrderController {
    @Autowired
    OrderService orderService;

    @RequestMapping(value = "/create",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public boolean createOrder(@RequestBody OrderDetails orderDetails){

        try {
            return orderService.createOrders(orderDetails);

        } catch (OrderCreationException e) {
            throw new HttpServerErrorException( HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
        } catch (NoInventoryException e) {
            throw new HttpServerErrorException(HttpStatus.ACCEPTED, e.getMessage());
        }

    }
}
