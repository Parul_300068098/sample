package com.vedantu.test.service;

import com.vedantu.test.dao.impl.ItemDao;
import com.vedantu.test.dao.impl.OrderDao;
import com.vedantu.test.entity.Item;
import com.vedantu.test.entity.OrderDetails;
import com.vedantu.test.entity.Orders;
import com.vedantu.test.exception.NoInventoryException;
import com.vedantu.test.exception.OrderCreationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpClientErrorException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class OrderService {

    public static volatile int counter=0;
    @Autowired
    ItemDao itemDao;
    @Autowired
    OrderDao orderDao;

    @Transactional
    public boolean  createOrders(OrderDetails orderDetails) throws OrderCreationException, NoInventoryException {

        if(orderDetails==null || orderDetails.getItemDetails()==null ) {
            throw new HttpClientErrorException(HttpStatus.INTERNAL_SERVER_ERROR,"OrderDetails are not available");
        }
        List<Item> items =itemDao.getItems(orderDetails.getItemDetails().keySet());

        // need to change the synchronization
        // for mongo transaction to work it should run on
        synchronized (items) {
            if (items.size() != orderDetails.getItemDetails().size()) {
                throw new NoInventoryException("Items are not available on inventory");
            }

            Orders orders= new Orders();
            orders.setCustomerId(orderDetails.getCustomerId());
            orders.setOrderId(OrderService.counter++);
            Double cost=0.0;

            Map<String, Integer> itemUpdates = new HashMap<>();
            for (Item item : items) {
                if (item.getQuantity() >= orderDetails.getItemDetails().get(item.getId()).getQuantity()) {
                    itemUpdates.put(item.getId(),
                            (item.getQuantity()-orderDetails.getItemDetails().get(item.getId()).getQuantity()));
                    cost =cost+item.getQuantity()* item.getPrice();
                    orders.getItems().add(item.getId());
                } else {
                    throw new NoInventoryException("Items are not available on inventory");
                }
            }

            orders.setTotalPrice(cost);

            try {
                boolean  result=itemDao.UpdateItems(itemUpdates);
                orderDao.insert(orders);
                return result;
            } catch (Exception e) {
                throw new OrderCreationException("Order Creation failed");
            }
        }

    }
}
