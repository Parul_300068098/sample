package com.vedantu.test.service;

import com.vedantu.test.dao.impl.ItemDao;
import com.vedantu.test.entity.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ItemService {

    @Autowired
    ItemDao itemDao;
    public void addItem(Item item) {
        itemDao.insert(item);
    }
}
