package com.vedantu.test.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Data
@Document(collection = "Orders")
public class Orders {

    @Id
    int orderId;
    // it should be auto sequence
    String CustomerId;
    List<String> items=new ArrayList<>();
    Double totalPrice;

}
