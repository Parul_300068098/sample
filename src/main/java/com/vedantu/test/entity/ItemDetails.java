package com.vedantu.test.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ItemDetails {
    @JsonProperty
    Double price;
    @JsonProperty
    Integer quantity;
}
