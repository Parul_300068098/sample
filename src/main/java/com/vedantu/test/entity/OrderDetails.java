package com.vedantu.test.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class OrderDetails {

    @JsonProperty
    String customerId;
    @JsonProperty
    Map<String,ItemDetails> itemDetails;
    //there might be thousands other fields like user attributes, Payment details, order details  This is the bare bone Details required
}
