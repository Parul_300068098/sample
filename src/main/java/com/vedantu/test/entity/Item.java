package com.vedantu.test.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "Inventory")
public class Item {

    @Id
    String id;
    String name;
    @JsonProperty
    volatile  int quantity;
    @JsonProperty
    double price;



}
