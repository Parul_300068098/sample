package com.vedantu.test.exception;

public class NoInventoryException extends Exception {

    public NoInventoryException(String msg) {
        super(msg);
    }
}
