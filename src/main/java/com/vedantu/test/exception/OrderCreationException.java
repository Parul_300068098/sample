package com.vedantu.test.exception;

public class OrderCreationException extends Exception{

    public OrderCreationException(String msg) {
        super(msg);
    }
}
