package com.vedantu.test.dao.impl;

import com.vedantu.test.entity.Item;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

@Component
public class ItemDao extends MongoDao<Item, String> {

    public List<Item> getItems(Collection<String> ids) {
        synchronized (ids) {
                 return super.getById(ids, Item.class);
        }
    }

    public boolean UpdateItems(Map<String, Integer> itemDetails) {
        List<Pair<Query, Update>> pairs = new ArrayList<>();

            for (Map.Entry<String, Integer> items : itemDetails.entrySet()) {
                Query query = new Query(Criteria.where("_id").is(items.getKey()));
                Update update = new Update();
                update.set("quantity", items.getValue());
                Pair<Query, Update> pair = Pair.of(query, update);
                pairs.add(pair);

                return super.update(pairs, Item.class);
            }
            return false;

    }
}
