package com.vedantu.test.dao.impl;

import com.vedantu.test.entity.OrderDetails;
import com.vedantu.test.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.BulkOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;
import java.util.Map;

@Component
public class MongoDao <T,U> {

    @Autowired
    MongoTemplate mongoTemplate;

    public T getById(U id, Class<T> type) {

        Query query = new Query(Criteria.where("_id").is(id));
        return mongoTemplate.findOne(query, type);
    }

    public List<T> getByQuery(Query query, Class<T> type) {
       return mongoTemplate.find(query,type);
    }

    public void upsert(Query query, Update update, Class<T> type) {
        update.set("lastUpdatedTimeStamp", DateUtils.getFormattedCurrentDateTime(null));
        mongoTemplate.upsert(query,update,type);
    }

    public void remove(T event) {
        mongoTemplate.remove(event);
    }

    public List<T> getById(Collection<U> ids, Class<T> type) {
        Query query = new Query(Criteria.where("_id").in(ids));
        return mongoTemplate.find(query, type);
    }

    public void save(T event) {
        mongoTemplate.save(event);
    }

    public void delete(Query query, Class<T> type) {
        mongoTemplate.remove(query,type);
    }


    public List<T> findAll(Class<OrderDetails> type) {
        return null;
    }

    public void insert(T orderDetails) {
        mongoTemplate.insert(orderDetails);
    }

    public void bulkInsert(List<T> events, Class<T> type) {
        BulkOperations bulkOperations =mongoTemplate.bulkOps(BulkOperations.BulkMode.UNORDERED, type);
        bulkOperations.insert(events);
        bulkOperations.execute();
    }

    public void remove(List<Query> removes, Class<T> type) {
        BulkOperations bulkOperations =mongoTemplate.bulkOps(BulkOperations.BulkMode.UNORDERED, type);
        bulkOperations.remove(removes);
        bulkOperations.execute();
    }


    public boolean update(List<Pair<Query, Update>> updates, Class<T> type) {
        BulkOperations bulkOperations =mongoTemplate.bulkOps(BulkOperations.BulkMode.UNORDERED, type);
        bulkOperations.updateMulti(updates);
        int updated= bulkOperations.execute().getModifiedCount();
        if(updated==updates.size()) {
            return true;
        } return false;
    }


}
